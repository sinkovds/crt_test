(function () {
  'use strict';

  angular.module('CRTApp')
    .controller('FilterNodesCtrl', ['$scope', '$animate', function ($scope, $animate) {

      $scope.admin_mode = false;

      $scope.selected_scope  = null;

        $scope.remove = function(scope) {
        scope.remove();
      };
        $(document).on('focus','#search_text',function(){
          $('.search_l').animate({
            opacity: 0.0
          },300);
          $('#search_text').animate({
            'padding-left': '5px'
          },300);
        });

        $scope.setAdminMode = function(mode){
          $('.tree-node').removeClass('active');
          $scope.selected_scope  = null;

          $scope.admin_mode = mode;
        };


        $(document).on('blur','#search_text',function(){
          $('.search_l').animate({
            opacity: 1
          },300);
          $('#search_text').animate({
            'padding-left': '28px'
          },300);
        });
        $scope.set_focus = function(){
          $('#search_text').focus();
        };
      $scope.newSubItem = function(scope) {
        var nodeData = scope.$modelValue;
        nodeData.nodes.push({
          id: nodeData.id * 10 + nodeData.nodes.length,
          title: nodeData.title + '.' + (nodeData.nodes.length + 1),
          nodes: []
        });
      };
        $scope.treeRoot = {
          dragStop: function(event){
            var object = $('#li_' + event.source.nodeScope.node.id);

            object.animate({
              right: "+=5px",
              opacity: 0.6
            }, 300 ).animate({
              right: "-=5px",
              opacity: 1
            }, 300 );

          }
        };

      $scope.visible = function(item) {

        if ($scope.query && $scope.query.length > 0
          && item.title.indexOf($scope.query) == -1) {
          $('#li_' + item.id).fadeOut("slow");

          return true;
        }
        $('#li_' + item.id).fadeIn("slow");
        return true;
      };

      $scope.findNodes = function(){

      };

      $scope.add_item = function(){

        if($scope.selected_scope!=null){
            $scope.newSubItem($scope.selected_scope);
        }

      };

      $scope.set_selected_scope = function(scope){

        if(!$scope.admin_mode)
          return false;
        $('.tree-node').removeClass('active');
        $(scope.$element).addClass('active');
        $scope.selected_scope = scope;
      };

      $scope.data = [{
        "id": 1,
        "title": "Element 1",
        "nodes": [
          {
            "id": 11,
            "title": "Element 1.1",
            "nodes": [
              {
                "id": 111,
                "title": "Element 1.1.1",
                "nodes": []
              }
            ]
          },
          {
            "id": 12,
            "title": "Element 1.2",
            "nodes": []
          }
        ],
      }, {
        "id": 2,
        "title": "Element 2",
        "nodes": [
          {
            "id": 21,
            "title": "Element 2.1",
            "nodes": []
          },
          {
            "id": 22,
            "title": "Element 2.2",
            "nodes": []
          }
        ],
      }, {
        "id": 3,
        "title": "Element 3",
        "nodes": [
          {
            "id": 31,
            "title": "Element 3.1",
            "nodes": []
          }
        ],
      }, {
        "id": 4,
        "title": "Element 4",
        "nodes": [
          {
            "id": 41,
            "title": "Element 4.1",
            "nodes": []
          }
        ],
      }];
    }]).directive('showitem', function ($compile) {
    return {
      link: function (scope, element, attrs) {
        $(element).animate({
          right: "+=5px",
          opacity: 0.6
        }, 300 ).animate({
          right: "-=5px",
          opacity: 1
        }, 300 );
      }
    }
  });;

}());