(function () {
  'use strict';

  angular.module('CRTApp', ['ui.tree', 'ngRoute'])

      .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
              controller: 'FilterNodesCtrl',
              templateUrl: 'views/filter-nodes.html'
            })
            .otherwise({
              redirectTo: '/'
            });
      }])
})();